//
// Created by pszemo on 05.12.2019.
//

#pragma once

#include "stm32l4xx_hal.h"
#include "mem_ptr.hpp"

namespace peripherals {
  constexpr uint32_t startup_delay        = 1000; //ms
  constexpr uint32_t led_interval_startup = 100;  //ms
  constexpr uint32_t boot_counter_address = 0x803fff0;

// pins & ports definitions
constexpr uint16_t    radio_rst_pin       = GPIO_PIN_0;
constexpr auto        radio_rst_port      = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    led_pin             = GPIO_PIN_0;
constexpr auto        led_port            = mem_ptr_t<GPIO_TypeDef>(GPIOB_BASE);
constexpr uint16_t    radio_tx_pin        = GPIO_PIN_2;
constexpr auto        radio_tx_port       = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    radio_rx_pin        = GPIO_PIN_3;
constexpr auto        radio_rx_port       = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    gps_tx_pin          = GPIO_PIN_9;
constexpr auto        gps_tx_port         = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    gps_rx_pin          = GPIO_PIN_10;
constexpr auto        gps_rx_port         = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    pt100_cs_pin        = GPIO_PIN_11;
constexpr auto        pt100_cs_port       = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    swdio_pin           = GPIO_PIN_13;
constexpr auto        swdio_port          = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);
constexpr uint16_t    swclk_pin           = GPIO_PIN_14;
constexpr auto        swclk_port          = mem_ptr_t<GPIO_TypeDef>(GPIOA_BASE);

// peripherals handlers
extern I2C_HandleTypeDef    hi2c1;
extern SPI_HandleTypeDef    hspi1;
extern UART_HandleTypeDef   huart1;
extern UART_HandleTypeDef   huart2;
extern IWDG_HandleTypeDef   hiwdg;
extern TIM_HandleTypeDef    htim1;

// init functions
void init();
void system_clock_config(void);
void gpio_init(void);
void i2c1_init(void);
void spi1_init(void);
void uart1_init(void);
void uart2_init(void);
void iwdg_init(void);
void increment_boot_counter();
void error_handler(void);
}


