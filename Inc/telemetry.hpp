//
// Created by pszemo on 14.12.2019.
//

#pragma once

#include <vector>
#include <cstdint>
#include "radio/radio_common.hpp"

namespace telemetry {
  constexpr uint32_t tm_frame_length = 64;

  constexpr uint32_t transfer_frame_data_length = 51;                       // mission specific, 64 TM frame - 13(first  header, second header)
  constexpr uint32_t spacecraft_identifier  = 1;                            // mission specific
  constexpr uint32_t transfer_frame_secondary_header_data_length = 6;       // mission specific
  constexpr char transfer_frame_secondary_header_data[] = "SP9FTL";         // mission/virtual channel specific
  constexpr bool tm_secondary_header = true;

  // virtual channels - mission specific
  constexpr uint32_t virtual_channel_count  = 1;
  constexpr uint32_t status_virtual_channel = 0;

  constexpr uint32_t transfer_frame_version = 0;
  constexpr uint32_t transfer_frame_secondary_header_version = 0;

  constexpr bool synchronization_flag = true;
  constexpr bool packet_order_flag = false;
  constexpr bool operational_control_field_flag = false;
  constexpr uint32_t segment_length_identifier = 0;
  constexpr uint32_t first_header_pointer = 0;

  namespace tm_frame{
    namespace primary_header {
      constexpr uint32_t transfer_frame_version_pos                  = 0;
      constexpr uint32_t transfer_frame_version_shift                = 6;
      constexpr uint32_t transfer_frame_version_mask                 = 0xC0;
      constexpr uint32_t spacecraft_identifier_pos1                  = 0;
      constexpr uint32_t spacecraft_identifier_pos2                  = 1;
      constexpr uint32_t spacecraft_identifier_shift2                = 4;
      constexpr uint32_t spacecraft_identifier_mask1                 = 0x3F;
      constexpr uint32_t spacecraft_identifier_mask2                 = 0xF0;
      constexpr uint32_t virtual_channel_identifier_pos              = 1;
      constexpr uint32_t virtual_channel_identifier_shift            = 1;
      constexpr uint32_t virtual_channel_identifier_mask             = 0x0E;
      constexpr uint32_t operational_control_frame_pos               = 1;
      constexpr uint32_t operational_control_frame_mask              = 0x01;
      constexpr uint32_t master_channel_frame_count_pos              = 2;
      constexpr uint32_t virtual_channel_frame_count_pos             = 3;
      constexpr uint32_t transfer_frame_secondary_header_flag_pos    = 4;
      constexpr uint32_t transfer_frame_secondary_header_flag_shift  = 7;
      constexpr uint32_t transfer_frame_secondary_header_flag_mask   = 0x80;
      constexpr uint32_t synchronization_flag_pos                    = 4;
      constexpr uint32_t synchronization_flag_shift                  = 6;
      constexpr uint32_t synchronization_flag_mask                   = 0x40;
      constexpr uint32_t packet_order_flag_pos                       = 4;
      constexpr uint32_t packet_order_flag_shift                     = 5;
      constexpr uint32_t packet_order_flag_mask                      = 0x20;
      constexpr uint32_t segment_length_identifier_pos               = 4;
      constexpr uint32_t segment_length_identifier_shift             = 3;
      constexpr uint32_t segment_length_identifier_mask              = 0x18;
      constexpr uint32_t first_header_pointer_pos1                   = 4;
      constexpr uint32_t first_header_pointer_pos2                   = 5;
      constexpr uint32_t first_header_pointer_mask1                  = 0x03;
    }
    namespace secondary_header {
      constexpr uint32_t version_number_pos    = 6;
      constexpr uint32_t version_number_shift  = 6;
      constexpr uint32_t version_number_mask   = 0xC0;
      constexpr uint32_t length_pos            = 6;
      constexpr uint32_t length_mask           = 0x3F;
      constexpr uint32_t data_field_pos        = 7;
    }
    constexpr uint32_t transfer_frame_data_pos = secondary_header::data_field_pos + transfer_frame_secondary_header_data_length;
  }

  namespace transfer_frame {
    namespace tm_bitmask {
      constexpr uint32_t pos                      = 0;
      constexpr uint32_t length                   = sizeof(uint16_t);
      constexpr uint16_t gps                      = 1;
      constexpr uint16_t pressure                 = 1 << 1;
      constexpr uint16_t ambient_temperature     = 1 << 2;
      constexpr uint16_t internal_temperature     = 1 << 3;
      constexpr uint16_t mcu_temperature          = 1 << 4;
      constexpr uint16_t humidity                 = 1 << 5;
      constexpr uint16_t muons                    = 1 << 6;
    };
    namespace boot_timestamp {
      constexpr uint32_t pos    = tm_bitmask::pos + tm_bitmask::length;
      constexpr uint32_t length = sizeof(float);
    };
    namespace boot_counter {
      constexpr uint32_t pos    = boot_timestamp::pos + boot_timestamp::length;
      constexpr uint32_t length = sizeof(float);
    };
    namespace gps {
      constexpr uint32_t pos      = boot_counter::pos + boot_counter::length;
      constexpr uint32_t length   = 16;
      constexpr uint32_t time_pos = 14;
      constexpr uint32_t fix_pos  = 12;
      constexpr uint32_t sats_pos = 0;
    }
    namespace pressure{
      constexpr uint32_t pos    = gps::pos + gps::length;
      constexpr uint32_t length = sizeof(float);
    }
    namespace ambient_temperature{
      constexpr uint32_t pos    = pressure::pos + pressure::length;
      constexpr uint32_t length = sizeof(float);
    }
    namespace internal_temperature{
      constexpr uint32_t pos    = ambient_temperature::pos + ambient_temperature::length;
      constexpr uint32_t length = sizeof(float);
    }
    namespace mcu_temperature{
      constexpr uint32_t pos    = internal_temperature::pos + internal_temperature::length;
      constexpr uint32_t length = sizeof(float);
    }
    namespace humidity{
      constexpr uint32_t pos    = mcu_temperature::pos + mcu_temperature::length;
      constexpr uint32_t length = sizeof(float);
    }
    namespace muons{
      constexpr uint32_t pos    = humidity::pos + humidity::length;
      constexpr uint32_t length = sizeof(float);
    }
  }

  class packet_manager_t {
  private:
    class tx_msg_handler_t {
    protected:
      tx_msg_handler_t* next;
    public:
      tx_msg_handler_t() : next(nullptr) {}
      virtual ~tx_msg_handler_t() = default;
      void set_next(tx_msg_handler_t* handler) { next = handler;}
      virtual void handle(radio::tx_msg_t& msg, uint8_t* buffer) = 0;
    };

    class gps_msg_handle_t : public tx_msg_handler_t {
    public:
      gps_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    class pressure_msg_handle_t : public tx_msg_handler_t {
    public:
      pressure_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    class external_temperature_msg_handle_t : public tx_msg_handler_t {
    public:
      external_temperature_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    class internal_temperature_msg_handle_t : public tx_msg_handler_t {
    public:
      internal_temperature_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    class mcu_temperature_msg_handle_t : public tx_msg_handler_t {
    public:
      mcu_temperature_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    class humidity_msg_handle_t : public tx_msg_handler_t {
    public:
      humidity_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    class muons_msg_handle_t : public tx_msg_handler_t {
    public:
      muons_msg_handle_t() = default;
      void handle(radio::tx_msg_t& msg, uint8_t* buffer) override;
    };

    std::vector<tx_msg_handler_t*>          handlers;
    uint8_t                                 buffer[tm_frame_length];
    uint8_t                                 master_channel_counter = 0;
    uint8_t                                 virtual_channel_counter[virtual_channel_count];

    void encode_primary_header(uint8_t virtual_channel);
    void encode_secondary_header();
    void encode_boot_timestamp(uint32_t timestamp);
    void encode_boot_number();

  public:
    packet_manager_t();
    ~packet_manager_t();
    void encode_packet(std::vector<radio::tx_msg_t>& tx_vector, uint32_t timestamp, uint8_t virtual_channel);
    const uint8_t* get_buffer() const;
  };
}
