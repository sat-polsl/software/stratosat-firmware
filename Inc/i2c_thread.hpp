//
// Created by pszemo on 10.12.2019.
//


#pragma once

#include "thread_base.hpp"

namespace radio{
  class radio_thread_t;
}

class i2c_thread_t : public thread_base_t<i2c_thread_t, 128> {
private:
  static constexpr char             name[]                          = "i2c_thread";
  static constexpr osPriority_t     priority                        = osPriorityNormal3;
  static constexpr uint32_t         measurement_interval            = 5000; // [ms]
  static constexpr uint16_t         mprls_address                   = 0x30;

  uint8_t                           i2c_buffer[4];

  static inline osSemaphoreId_t     i2c_semaphore                   = nullptr;
  osStaticSemaphore_t               i2c_semaphore_control_block;

  radio::radio_thread_t*            radio_thread                    = nullptr;

public:

  i2c_thread_t(radio::radio_thread_t *radio_thread);
  void thread();
  const char* get_name() const;
  const osPriority_t get_priority() const;
  static osSemaphoreId_t get_i2c_semaphore();
};


