
/* Includes ------------------------------------------------------------------*/
#include "cmsis_os.h"
#include "peripherals.hpp"
#include "gps_thread.hpp"
#include "radio/radio_thread.hpp"
#include "i2c_thread.hpp"
#include "spi_thread.hpp"

radio::radio_thread_t radio_thread;
gps_thread_t gps_thread(&radio_thread);
i2c_thread_t i2c_thread(&radio_thread);
spi_thread_t spi_thread(&radio_thread);
extern uint32_t boot_counter;


/* Private function prototypes -----------------------------------------------*/

void *operator new(size_t size) {
  return pvPortMalloc(size);
}

void *operator new[](size_t size) {
  return pvPortMalloc(size);
}

void operator delete(void *ptr) {
  vPortFree(ptr);
}

void operator delete[](void *ptr) {
  vPortFree(ptr);
}

int main(void) {
  peripherals::init();

  osKernelInitialize();

  radio_thread.start();
  gps_thread.start();
  i2c_thread.start();
  spi_thread.start();

  osKernelStart();

  while (1) {}
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
