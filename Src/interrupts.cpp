//
// Created by pszemo on 11.12.2019.
//

#include "interrupts.hpp"
#include "peripherals.hpp"
#include "gps_thread.hpp"
#include "radio/radio_thread.hpp"
#include "radio/radio_controller.hpp"
#include "i2c_thread.hpp"
#include "spi_thread.hpp"

using namespace radio;

/////////// FAULT HANDLERS /////////////

void NMI_Handler(void) {}

void HardFault_Handler(void) {
  while(1) {}
}

void MemManage_Handler(void) {
  while (1) {
  }
}

void BusFault_Handler(void) {
  while (1) {
  }
}

void UsageFault_Handler(void) {
  while (1) {
  }
}

void DebugMon_Handler(void) {}

//////////// IRQ HANDLERS ////////////

void TIM1_UP_TIM16_IRQHandler(void) {
  HAL_TIM_IRQHandler(&peripherals::htim1);
}

void USART1_IRQHandler(void) {
  HAL_UART_IRQHandler(&peripherals::huart1);
}

void USART2_IRQHandler(void) {
  HAL_UART_IRQHandler(&peripherals::huart2);
}

void I2C1_EV_IRQHandler(void) {
  HAL_I2C_EV_IRQHandler(&peripherals::hi2c1);
}

void I2C1_ER_IRQHandler(void) {
  HAL_I2C_ER_IRQHandler(&peripherals::hi2c1);
}

void SPI1_IRQHandler(void) {
  HAL_SPI_IRQHandler(&peripherals::hspi1);
}

/////////// CALLBACKS ////////////////

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
  if (huart->Instance == USART1) {
    osMessageQueueId_t queue = gps_thread_t::get_uart_queue();
    if (queue != nullptr) {
      osMessageQueuePut(queue, &gps_thread_t::uart_byte, 0, 0);
    }
    // GPS restart detection
    if (gps_thread_t::uart_byte) {
      HAL_UART_Receive_IT(huart, &gps_thread_t::uart_byte, 1);
    }
  } else if (huart->Instance == USART2) {
    osMessageQueueId_t queue = radio_thread_t::get_uart_queue();
    if (queue != nullptr) {
      osMessageQueuePut(queue, &radio_controller_t::uart_byte, 0, 0);
    }
    HAL_UART_Receive_IT(huart, &radio_controller_t::uart_byte, 1);
  }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart) {
  if (huart->Instance == USART2) {
    osMessageQueueId_t queue = radio_thread_t::get_uart_queue();
    if (queue != nullptr) {
      osMessageQueuePut(queue, &radio_controller_t::uart_byte, 0, 0);
    }
  }
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef* hi2c) {
  if (hi2c->Instance = I2C1) {
    osSemaphoreId_t semaphore = i2c_thread_t::get_i2c_semaphore();
    if (semaphore != nullptr) {
      osSemaphoreRelease(semaphore);
    }
  }
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef* hi2c) {}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef* hspi) {
  if (hspi->Instance == SPI1) {
    osSemaphoreId_t semaphore = spi_thread_t::get_spi_semaphore();
    if (semaphore != nullptr) {
      osSemaphoreRelease(semaphore);
    }
  }
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef* hspi) {
  if (hspi->Instance == SPI1) {
    osSemaphoreId_t semaphore = spi_thread_t::get_spi_semaphore();
    if (semaphore != nullptr) {
      osSemaphoreRelease(semaphore);
    }
  }
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef* hspi) {
  if (hspi->Instance == SPI1) {
    osSemaphoreId_t semaphore = spi_thread_t::get_spi_semaphore();
    if (semaphore != nullptr) {
      osSemaphoreRelease(semaphore);
      HAL_GPIO_WritePin(peripherals::pt100_cs_port, peripherals::pt100_cs_pin, GPIO_PIN_SET);
    }
  }
}

