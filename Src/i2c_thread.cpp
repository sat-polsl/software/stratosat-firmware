//
// Created by pszemo on 10.12.2019.
//

#include "i2c_thread.hpp"
#include "peripherals.hpp"
#include "radio/radio_common.hpp"
#include "radio/radio_thread.hpp"

i2c_thread_t::i2c_thread_t(radio::radio_thread_t *radio_thread)
: radio_thread(radio_thread) {
  const osSemaphoreAttr_t sem_attr = {
    .name = "i2c sem",
    .attr_bits = 0,
    .cb_mem = &i2c_semaphore_control_block,
    .cb_size = sizeof(i2c_semaphore_control_block)
  };
  i2c_semaphore = osSemaphoreNew(1, 0, &sem_attr);
}

void i2c_thread_t::thread() {
  uint32_t measurement_start;
  uint32_t measurement_end;
  osStatus_t status;

  for (;;) {
    measurement_start = osKernelGetTickCount();
    i2c_buffer[0] = 0xAA;
    i2c_buffer[1] = 0x00;
    i2c_buffer[2] = 0x00;
    HAL_I2C_Master_Transmit_IT(&peripherals::hi2c1, mprls_address, i2c_buffer, 3);
    osDelay(6);
    HAL_I2C_Master_Receive_IT(&peripherals::hi2c1, mprls_address, i2c_buffer, 4);
    status = osSemaphoreAcquire(i2c_semaphore, 1000);
    if (status == osOK) {
      uint32_t val = i2c_buffer[1] << 16 | i2c_buffer[2] << 8 | i2c_buffer[3];
      val = (val - 1677722) * 25;
      float pressure = static_cast<float>(val)/13421772.0*6894.75729;
      radio::single_measurement_t* measurement = new radio::single_measurement_t{pressure};
      radio::tx_msg_t msg{osKernelGetTickCount(), radio::tx_type::pressure, measurement};
      osMessageQueuePut(radio_thread->get_tx_queue(), &msg, 0, 500);
    }

    // TODO: AM2320 communication

    measurement_end = osKernelGetTickCount();
    osDelay(measurement_interval - (measurement_end - measurement_start));
  }
}

const char* i2c_thread_t::get_name() const {
  return name;
}

const osPriority_t i2c_thread_t::get_priority() const {
  return priority;
}

osSemaphoreId_t i2c_thread_t::get_i2c_semaphore() {
  return i2c_semaphore;
}
