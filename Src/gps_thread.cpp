//
// Created by pszemo on 09.12.2019.
//

#include "gps_thread.hpp"
#include "nmeaparse/nmea.h"
#include "peripherals.hpp"
#include "radio/radio_thread.hpp"

gps_thread_t::gps_thread_t(radio::radio_thread_t* radio_thread)
: radio_thread(radio_thread) {
  const osMessageQueueAttr_t attr = {
    .name = "gps_uart_queue",
    .attr_bits = 0,
    .cb_mem = &uart_queue_control_block,
    .cb_size = sizeof(uart_queue_control_block),
    .mq_mem = &uart_queue_buffer,
    .mq_size = sizeof(uart_queue_buffer)
  };
  uart_queue = osMessageQueueNew(uart_queue_size, sizeof(uint8_t), &attr);
}

void gps_thread_t::thread() {
  HAL_UART_Receive_IT(&peripherals::huart1, &uart_byte, 1);
  uint8_t gps_char;
  nmea::NMEAParser parser;
  parser.onSentence += [this](const nmea::NMEASentence &n) {
    if (n.valid() && n.name == "GPGGA" && n.checksumOK() && n.parameters.size() == 14) {
      nmea::GPSFix *fix = new nmea::GPSFix;
      fix->timestamp.setTime(nmea::parseFloat(n.parameters[nmea::gpgga::timestamp]));
      fix->latitude = nmea::convertLatLongToDeg(n.parameters[nmea::gpgga::latitude],
                                                n.parameters[nmea::gpgga::latitude_dir]);
      fix->longitude = nmea::convertLatLongToDeg(n.parameters[nmea::gpgga::longitude],
                                                 n.parameters[nmea::gpgga::longitude_dir]);
      fix->quality = static_cast<uint8_t>(nmea::parseInt(n.parameters[nmea::gpgga::fix_quality]));
      fix->tracking_satellites = static_cast<int32_t>(nmea::parseInt(n.parameters[nmea::gpgga::tracked_satellites]));
      fix->altitude = nmea::parseFloat(n.parameters[nmea::gpgga::altitude]);
      if (fix->quality) {
        radio::tx_msg_t msg{osKernelGetTickCount(), radio::tx_type::gps, fix};
        osMessageQueuePut(radio_thread->get_tx_queue(), &msg, 0, 500);
      }
    }
  };
  for (;;) {
    if (osMessageQueueGet(uart_queue, &gps_char, 0, osWaitForever) == osOK) {
      if (gps_char == 0x00) {
        /*
         * GPS GY-GPS6MV2 sends total garbage after restart and that garbage hangs MCU.
         * To prevent this UART1 is disabled after detection of 0x00 in receive register (0x00 is send by GPS after restart)
         * Then restart UART1 after 5 seconds
         */
        HAL_UART_DeInit(&peripherals::huart1);
        osDelay(5000);
        osMessageQueueReset(uart_queue);
        peripherals::uart1_init();
        HAL_UART_Receive_IT(&peripherals::huart1, &uart_byte, 1);
      } else {
        parser.readByte(gps_char);
      }
    }
  }
}

osMessageQueueId_t gps_thread_t::get_uart_queue() {
  return uart_queue;
}

const char *gps_thread_t::get_name() const {
  return name;
}

const osPriority_t gps_thread_t::get_priority() const {
  return priority;
}

