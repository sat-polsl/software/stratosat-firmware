//
// Created by pszemo on 14.12.2019.
//

#include "telemetry.hpp"
#include "radio/radio_common.hpp"
#include "nmeaparse/NMEAParser.h"
#include "peripherals.hpp"
#include <algorithm>

void telemetry::packet_manager_t::gps_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::gps) {
    nmea::GPSFix* fix = static_cast<nmea::GPSFix*>(msg.data);
    std::copy(
      reinterpret_cast<uint8_t*>(&fix->latitude),
      reinterpret_cast<uint8_t*>(&fix->latitude) + 3*sizeof(float),
      buffer + tm_frame::transfer_frame_data_pos + transfer_frame::gps::pos
    );
    uint32_t time_fix_pos = static_cast<uint32_t >(fix->timestamp.rawTime) << transfer_frame::gps::time_pos |
      fix->quality << transfer_frame::gps::fix_pos |
      fix->tracking_satellites << transfer_frame::gps::sats_pos;

    std::copy(
      reinterpret_cast<uint8_t*>(&time_fix_pos),
      reinterpret_cast<uint8_t*>(&time_fix_pos) + sizeof(uint64_t),
      buffer + tm_frame::transfer_frame_data_pos + transfer_frame::gps::pos + 3*sizeof(float)
    );


    uint16_t* bitmask = reinterpret_cast<uint16_t*>(buffer + tm_frame::transfer_frame_data_pos + transfer_frame::tm_bitmask::pos);
    *bitmask |= transfer_frame::tm_bitmask::gps;
  } else {
    next->handle(msg, buffer);
  }
}

void telemetry::packet_manager_t::pressure_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::pressure) {
    radio::single_measurement_t* pressure = static_cast<radio::single_measurement_t*>(msg.data);
    std::copy(
      reinterpret_cast<uint8_t*>(&pressure->val),
      reinterpret_cast<uint8_t*>(&pressure->val) + transfer_frame::pressure::length,
      buffer + tm_frame::transfer_frame_data_pos + transfer_frame::pressure::pos
    );
    uint16_t* bitmask = reinterpret_cast<uint16_t*>(buffer + tm_frame::transfer_frame_data_pos + transfer_frame::tm_bitmask::pos);
    *bitmask |= transfer_frame::tm_bitmask::pressure;
  } else {
    next->handle(msg, buffer);
  }
}

void telemetry::packet_manager_t::external_temperature_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::ambient_temperature) {
    radio::single_measurement_t* ambient_temperature = static_cast<radio::single_measurement_t*>(msg.data);
    std::copy(
      reinterpret_cast<uint8_t*>(&ambient_temperature->val),
      reinterpret_cast<uint8_t*>(&ambient_temperature->val) + transfer_frame::ambient_temperature::length,
      buffer + tm_frame::transfer_frame_data_pos + transfer_frame::ambient_temperature::pos
    );
    uint16_t* bitmask = reinterpret_cast<uint16_t*>(buffer + tm_frame::transfer_frame_data_pos + transfer_frame::tm_bitmask::pos);
    *bitmask |= transfer_frame::tm_bitmask::ambient_temperature;
  } else {
    next->handle(msg, buffer);
  }
}

void telemetry::packet_manager_t::internal_temperature_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::internal_temperature) {

  } else {
    next->handle(msg, buffer);
  }
}

void telemetry::packet_manager_t::mcu_temperature_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::mcu_temperature) {

  } else {
    next->handle(msg, buffer);
  }
}

void telemetry::packet_manager_t::humidity_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::humidity) {

  } else {
    next->handle(msg, buffer);
  }
}

void telemetry::packet_manager_t::muons_msg_handle_t::handle(radio::tx_msg_t &msg, uint8_t *buffer) {
  if (msg.type == radio::tx_type::muons) {

  } else {
    next->handle(msg, buffer);
  }
}

telemetry::packet_manager_t::packet_manager_t() {
  handlers.push_back(new gps_msg_handle_t());
  handlers.push_back(new pressure_msg_handle_t());
  handlers.push_back(new external_temperature_msg_handle_t());
  handlers.push_back(new internal_temperature_msg_handle_t());
  handlers.push_back(new mcu_temperature_msg_handle_t());
  handlers.push_back(new humidity_msg_handle_t());
  handlers.push_back(new muons_msg_handle_t());
  for (auto it = handlers.begin(); it != handlers.end() - 1; ++it){
    (*it)->set_next(*(it+1));
  }
  std::fill(virtual_channel_counter, virtual_channel_counter + virtual_channel_count, 0);
}

telemetry::packet_manager_t::~packet_manager_t() {
  for (auto it = handlers.begin(); it != handlers.end(); ++it){
    delete *it;
  }
}

void telemetry::packet_manager_t::encode_packet(std::vector<radio::tx_msg_t> &tx_vector, uint32_t timestamp, uint8_t virtual_channel) {
  std::fill(buffer, buffer + tm_frame_length, 0);
  encode_primary_header(virtual_channel);
  encode_secondary_header();
  encode_boot_timestamp(timestamp);
  encode_boot_number();

  for(auto it = tx_vector.begin(); it != tx_vector.end(); ++it) {
    handlers.front()->handle(*it, buffer);
  }

}

void telemetry::packet_manager_t::encode_primary_header(uint8_t virtual_channel) {
  buffer[tm_frame::primary_header::transfer_frame_version_pos] = (1 << tm_frame::primary_header::transfer_frame_version_shift);
  buffer[tm_frame::primary_header::spacecraft_identifier_pos1] |= spacecraft_identifier >> 2;
  buffer[tm_frame::primary_header::spacecraft_identifier_pos2] = (spacecraft_identifier & 0x0F) << tm_frame::primary_header::spacecraft_identifier_shift2;
  buffer[tm_frame::primary_header::virtual_channel_identifier_pos] |= virtual_channel << tm_frame::primary_header::virtual_channel_identifier_shift;
  buffer[tm_frame::primary_header::operational_control_frame_pos] |= operational_control_field_flag;
  buffer[tm_frame::primary_header::master_channel_frame_count_pos] = master_channel_counter;
  buffer[tm_frame::primary_header::virtual_channel_frame_count_pos] = virtual_channel_counter[virtual_channel];
  buffer[tm_frame::primary_header::transfer_frame_secondary_header_flag_pos] = (tm_secondary_header ? 1 : 0) << tm_frame::primary_header::transfer_frame_secondary_header_flag_shift;
  buffer[tm_frame::primary_header::synchronization_flag_pos] |= synchronization_flag << tm_frame::primary_header::synchronization_flag_shift;
  buffer[tm_frame::primary_header::packet_order_flag_pos] |= packet_order_flag << tm_frame::primary_header::packet_order_flag_shift;
  buffer[tm_frame::primary_header::segment_length_identifier_pos] |= segment_length_identifier << tm_frame::primary_header::segment_length_identifier_shift;
  buffer[tm_frame::primary_header::first_header_pointer_pos1] |= (first_header_pointer & 0x03);
  buffer[tm_frame::primary_header::first_header_pointer_pos2] = 0xFF & first_header_pointer;

  master_channel_counter++;
  virtual_channel_counter[virtual_channel]++;
}

void telemetry::packet_manager_t::encode_secondary_header() {
  buffer[tm_frame::secondary_header::version_number_pos] = transfer_frame_secondary_header_version << tm_frame::secondary_header::version_number_shift;
  buffer[tm_frame::secondary_header::length_pos] = transfer_frame_secondary_header_data_length & tm_frame::secondary_header::length_mask;
  std::copy(
    transfer_frame_secondary_header_data,
    transfer_frame_secondary_header_data + transfer_frame_secondary_header_data_length,
    buffer + tm_frame::secondary_header::data_field_pos
  );
}

void telemetry::packet_manager_t::encode_boot_timestamp(uint32_t timestamp) {
  std::copy(
    reinterpret_cast<uint8_t*>(&timestamp),
    reinterpret_cast<uint8_t*>(&timestamp) + transfer_frame::boot_timestamp::length,
    buffer + tm_frame::transfer_frame_data_pos + transfer_frame::boot_timestamp::pos
    );
}

const uint8_t* telemetry::packet_manager_t::get_buffer() const {
  return buffer;
}

void telemetry::packet_manager_t::encode_boot_number() {
  uint32_t boot_counter = *(uint32_t*)peripherals::boot_counter_address;
  std::copy(
    reinterpret_cast<uint8_t*>(&boot_counter),
    reinterpret_cast<uint8_t*>(&boot_counter) + transfer_frame::boot_counter::length,
    buffer + tm_frame::transfer_frame_data_pos + transfer_frame::boot_counter::pos
  );
}




